package casir.matchgame;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.Exception;

class MatchGameTest {

    @BeforeEach // this function is called before each test
    void setUp(){
        // init context
    }

    @Test // this is a test
    void addTwoInt() throws Exception {
        int c = MatchGame.add(2, 1);
        assertEquals(3, c); // see also assertTrue, assertContains...
    }

    @Test // this test is valid if the expected exception is thrown
    void addSumIs0() {
        Executable sum = () -> {int c = MatchGame.add(-1, 1);};
        assertThrows(Exception.class, sum);
    }
    
    @Test
    void isUserInputSup3() {
    	Matches match = new Matches();
    	boolean b = match.isUserInputValid(4);
    	assertFalse(b);   	
    }
    
    @Test
    void isUserInputEqual0() {
    	Matches match = new Matches();
    	boolean b = match.isUserInputValid(0);
    	assertFalse(b);
    }
    
    @Test
    void isUserInputBetween1And3() {
    	Matches match = new Matches();
    	boolean b = match.isUserInputValid(1);
    	assertTrue(b);
    }
    
    @Test
    void numberMatchesEqual1() {
    	Matches match = new Matches();
    	boolean b = match.matchIsFinished(1);
    	assertTrue(b);
    }
    
    @Test
    void numberMatchesEqual0() {
    	Matches match = new Matches();
    	boolean b = match.matchIsFinished(0);
    	assertTrue(b);
    }
    
    @Test
    void numberMatchesSup1() {
    	Matches match = new Matches();
    	boolean b = match.matchIsFinished(2);
    	assertFalse(b);
    }

}
