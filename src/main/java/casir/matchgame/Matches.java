package casir.matchgame;

import java.lang.Exception;
import java.util.Scanner;
import java.util.Random;

public class Matches {
	
	public int remaining = 10;

    public Matches(){
    }
 
    public void run(){
        while (true) {
            Scanner reader = new Scanner(System.in);
            System.out.println("Enter a number between 1 and 3: ");
            int n = reader.nextInt();
            while(!this.isUserInputValid(n)) {
            	System.out.println("Invalid number. Enter a number between 1 and 3: ");
                n = reader.nextInt();
            }
            this.remaining -= n;
            if (this.remaining == 1){
                System.out.println("You Win");
                break;
            }
            System.out.println("Remaining " + this.remaining);
            n = new Random().nextInt(2) + 1;
            this.remaining -= n;
            if (this.remaining == 1){
                System.out.println("You Loose");
                break;
            }
            System.out.println("Remaining " + this.remaining);
        }
    }
    
    
    public Boolean isUserInputValid(int i){
    	
    	if (i>this.remaining) {
    		return false;
    	}
//    	int i = Integer.valueOf(n);    
        if(i >=4 || i <=0) {
        	return false;
        }
        else {
        	return true;
        }
    }
    
    public Boolean matchIsFinished(int n) {  	
    	if(n == 0 || n == 1) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
}